"""Script to optimize the selection of interfaces and flow distribution
using the Sedeno-Noda BiOptimizating Algorithm"""

#####################################################################
# Federico Cavallo (2016)
# Implementation for BiObjective Optimization
# with the algorithm of Sedeno-Noda
# [1]Sedeno-Noda - An algorithm for the biobjective integer minimum 
# cost flow problem
#####################################################################

import sys

#for developping, after making setup will be removed
sys.path.append('/home/federico/Dropbox/1_Thesis/10_Flow_path_algorithm/2_repository')
sys.path.append('/home/federico/Dropbox/1_Thesis/12_MADM_Algorithm/')
#

import networkx as nx
import utils.networksimplex as bi_ns
import utils.brute_force as bt
import MADM
import numpy as np
from collections import deque
import copy
import random as rd
import matplotlib.pyplot as plt

#constans
R_C = 7
R_C2 = 8
cons_C = 5
cons_C2 = 6

def main():
    """Main entry point for the script."""
    
    #new_net_simplex_examples()

    #Network oriented problem
    if False:
        m = 3 #number of networks
        n = 3 #number of criteria for MADM
        h = 3 #number of apps
        #Network matrix
        D = np.array([(150,55,10e-4), 
                    (180,65,10e-4),
                    (100, 50,10e-3)]) #delay, jitter, error rate (n x m)
        B = [64,64,64] #Bandwith of each network
        print D
        print B
        #Vector indicating if it is a parameter to maximize or minimize
        max_or_min = [] # all mins
        for i in range(n):
            max_or_min.append(0)
        #TOPSIS
        W = []
        r = []
        rank = []
        W1 = []
        W1 = W1/np.sum(W1)
        
        
        W1 = [0.2,0.3,0.5]
        W2 = [0.15,0.2,0.65]
        W3 = [0.1,0.2,0.7]
        r1 = MADM.TOPSIS(n, m, W1, D, max_or_min,ret='r')
        r2 = MADM.TOPSIS(n, m, W2, D, max_or_min,ret='r')
        r3 = MADM.TOPSIS(n, m, W3, D, max_or_min,ret='r')
        rank1 = MADM.TOPSIS(n, m, W1, D, max_or_min)
        rank2 = MADM.TOPSIS(n, m, W2, D, max_or_min)
        rank3 = MADM.TOPSIS(n, m, W3, D, max_or_min)
        W = [W1,W2,W3]
        r = [r1,r2,r3]
        print np.min(r)
        r = np.max(r)-r+1
        r = W
        rank= [rank1,rank2,rank3]
        for app_w in W:    
            print app_w
        for app_r in r:    
            print app_r
        for app_rank in rank:
            print app_rank

        nn = 7 #amount of nodes
        app_nodes = [1,2,3]
        network_nodes = [4,5,6]
        in_node = 7
        out_node = 0
        edges = []
        max_cost = 10
        C = []
        C2 = []
        u = []
        d = []
        F = []
        T = []
        B_demand = [40,40,40] #Demanded bandwith
        number_of_apps = len(app_nodes)
        for a_node in app_nodes:
            for n_node in network_nodes:
                edges.append((a_node,n_node))
                C.append(r[a_node-1][n_node-m-1])    
                C2.append(0)
                u.append(1)
                F.append(a_node)
                T.append(n_node)
        for n_node in network_nodes:
            edges.append((n_node,out_node))
            C.append(0)
            C2.append(1)
            u.append(number_of_apps)
            F.append(n_node)
            T.append(out_node)
        #print C
        #print C2
        #print u
        problem = bi_ns.Problem(nn,edges,C,C2,u,d)
        problem.network_simplex(app_nodes,network_nodes,in_node,out_node)
        print problem.points[0].x
        problem.calculate_C2()
        EEP = IPOEB2(problem,F,T,C,C2,ex=1)
        EIP(EEP)
        brute_solution1 = bt.brute_force(h,m,r,np.ones((h,m)),[64,64,64],[40,40,40])
        #brute_solution1 = bt.brute_force(10,4,np.random.rand(10,4),np.random.rand(10,4),[400]*4,[40]*10)
        print brute_solution1[0]
        ##########################################
        #Second Example
    if False:
        ex = 2
        m = 3 #number of networks
        n = 3 #number of criteria for MADM
        h = 3 #number of apps
        #Network matrix
        W1 = [0.5,0.3,0.2]
        W2 = [0.5,0.2,0.3]
        W3 = [0.4,0.2,0.4]
        #W1 = [0.2,0.3,0.5]
        #W2 = [0.2,0.3,0.5]
        #W3 = [0.2,0.3,0.5]
        r = [W1,W2,W3]
        nn = 7 #amount of nodes
        app_nodes = [1,2,3]
        network_nodes = [4,5,6]
        in_node = 7 #does not exist in fact
        out_node = 0
        edges = []
        max_cost = 10
        C = []
        C2 = []
        u = []
        d = []
        F = []
        T = []
        number_of_apps = len(app_nodes)
        for a_node in app_nodes:
            for n_node in network_nodes:
                edges.append((a_node,n_node))
                C.append(r[a_node-1][n_node-m-1])    
                C2.append(0)
                u.append(1)
                F.append(a_node)
                T.append(n_node)
        for n_node in network_nodes:
            edges.append((n_node,out_node))
            C.append(0)
            C2.append(1)
            u.append(number_of_apps)
            F.append(n_node)
            T.append(out_node)
        problem = bi_ns.Problem(nn,edges,C,C2,u,d)
        problem.network_simplex(app_nodes,network_nodes,in_node,out_node)
        print problem.points[0].x
        problem.calculate_C2(ex=2)
        #EEP2 = IPOEB2(problem,F,T,C,C2,ex=2)
        #EIP(EEP2)
        brute_solution2 = bt.brute_force(h,m,r,np.ones((h,m)),[2400, 1300, 1000],[500,900,1400])
        #brute_solution2 = bt.brute_force(h,m,r,np.ones((h,m)),[1600, 1000, 1400],[500,900,1400])
        print brute_solution2[0]
    if False:
        print "##################################"
        print "Network oriented problem"
        print "Efficient points Network problem"
        for point in EEP:
            print "f1,f2:(", point.flow_cost1,",",point.flow_cost2,")"
        for point in EEP:
            print point.new_edges_x
            print point.new_edges_C2
        plot_function(EEP)
        plot_brute_solutions(brute_solution1)
        
    if False:
        print "##################################"
        print "Example 2"

        print "Efficient points Network problem"
        for point in EEP2:
            print "f1,f2:(", point.flow_cost1,",",point.flow_cost2,")"
        for point in EEP2:
            print point.new_edges_x
            print point.new_edges_C2

        
        plot_function(EEP2)
    #plot_brute_solutions(brute_solution2)
    #plt.show()

    #exit()
    old_examples()

def IPOEB(G):
    """Algorithm that finds the integer points on
    the eficient boundary"""

    #Solving first efficient point by network siplex
    
    eff_1st_point, F, T, U, C, C2 = bi_ns.network_simplex(G) #F: from,U: capacity, C:cost1, C2:cost2
    eff_1st_point.calculate_reduced_costs()
    eff_1st_point.define_non_tree_arcs()
    eff_1st_point.compute_flow_cost()
    EEP = [] #Efficient Extreme Points
    R = [] #Efficient points for checking
    #eff_red_1st = bi_ns.Reduced_Efficient_Extreme_Point(eff_1st_point)
    #EEP.append(eff_red_1st)
    EEP.append(eff_1st_point)
    #print "X:"
    #print eff_1st_point.new_edges_x
    R = [eff_1st_point]
    while R:
        S = []
        ext_point = R.pop(0)
        S = compute_entering_arcs(ext_point,ex=0)
        #print "R:", ext_point.R
        new_points = compute_new_points(ext_point,S,EEP,R,ex=0)
    return EEP

def IPOEB2(problem,F,T,C,C2,ex):
    """Algorithm that finds the integer points on
    the eficient boundary"""

    #Solving first efficient point by network siplex
    eff_1st_point = bi_ns.Efficient_Extreme_Point()
    print problem.C2
    eff_1st_point.initialize_from_other_new_point(problem,F,T,C,C2)
    eff_1st_point.calculate_reduced_costs()
    eff_1st_point.compute_flow_cost()
    EEP = [] #Efficient Extreme Points
    R = [] #Efficient points for checking
    EEP.append(eff_1st_point)
    R = [eff_1st_point]
    while R:
        S = []
        ext_point = R.pop(0)
        S = compute_entering_arcs(ext_point,ex)
        new_points = compute_new_points(ext_point,S,EEP,R,ex)
    return EEP

def EIP(EEP):
    """Algorithm that find the efficient integer points that
    do not lie in the efficient bundary"""
    List = EEP
    point = find_candidate_arc(List)
    if not point:
        return
    while point:
        if non_dominated_R(point,List):
            #print point.flow_cost1, point.flow_cost2
            #print point.R[0][3]
            #print point.flow_cost1 + point.R[0][1]
            (leaving_arc,delta,_), pivot_cycle = point.find_path(list(zip(point.F,point.T)).index(point.R[0][3]),point.reduced_cost1[list(zip(point.F,point.T)).index(point.R[0][3])]) #returned leaving arc and delta
            #print "Delta:",delta
            if delta > 0:
                #print "in"
                new_point = bi_ns.Efficient_Extreme_Point()
                new_point.initialize_from_other_point(point)
                new_point.send_one_unit_flow(pivot_cycle)
                new_point.compute_flow_cost()
                #print "B", new_point.B 
                #print "L"
                ##for e in new_point.L:
                    #print "(", new_point.F[e],",",new_point.T[e],")"
                #print "U"
                #for e in new_point.U:
                    #print "(", new_point.F[e],",",new_point.T[e],")"
                #print new_point.flow_cost1
                new_index = List.index(point)+1
                while List[new_index].flow_cost1 < new_point.flow_cost1:
                    new_index +=1
                List.insert(new_index,new_point)
        point.R.pop(0)
        point = find_candidate_arc(List)
        

def compute_entering_arcs(aux,ex):
    """Computing entering arcs for ipoeb"""
    relation = None
    S = []
    #print aux.L
    s = [] #slope rc2_/rc1_, R arcs for EIP
    aux.calculate_C2(ex=ex)
    for edge in aux.L:
        rc1 = float(aux.reduced_cost1[edge])
        rc2 = float(aux.reduced_cost2[edge])
        if rc2 < 0: 
            if rc1 != 0:
                new_relation = rc2/rc1
            else:
                new_relation = rc2/0.01 #zero division patch
            if rc1 > 0:
                s.append([new_relation,rc1,rc2,(aux.F[edge],aux.T[edge])])
            
            #print "Edge:", aux.arc_num_to_arc_nodes(edge), "New relation:", new_relation
            #print "Red_cost1,Red_cost2", rc1,rc2
            if new_relation < relation or relation == None:
                S = []
                S.append(edge)
                relation = new_relation
            elif relation == new_relation:
                S.append(edge)

    for edge in aux.U:
        rc1 = float(aux.reduced_cost1[edge])
        rc2 = float(aux.reduced_cost2[edge])
        if rc2 > 0:
            if rc1 != 0:
                new_relation = rc2/rc1
            else:
                new_relation = rc2/0.01 #zero division patch
            if rc1 < 0:
                s.append([new_relation, -rc1,-rc2, (aux.F[edge],aux.T[edge])])
            if new_relation < relation or relation == None:
                S = []
                S.append(edge)
                relation = new_relation
            elif relation == new_relation:
                S.append(edge)


    for edge in s:
        if edge[0] == relation:
            s.remove(edge)
    #print "R in compute_entering_arcs(after):", s
    aux.R = s # Define R for EIP algorithm
    s.sort(key = lambda edge: edge[1])
    #print "S:"
    #for e in  S:
        #print aux.arc_num_to_arc_nodes(e)
    return S


def compute_new_points(ext_point, S, EEP, R,ex):
    """Computing new points for ipoeb"""
    for entering_arc in S:
        new_point = bi_ns.Efficient_Extreme_Point()
        new_point.initialize_from_other_point(ext_point)
        #print "S;",S
        #print new_point.B
        #print new_point.arc_num_to_arc_nodes(entering_arc)        
        #print "depth ",new_point.depth
        #print "thread",new_point.thread
        #print "pred  ",new_point.pred
        
        (leaving_arc,delta,_), pivot_cycle = new_point.find_cycle(entering_arc) #returned leaving arc and delta
        print "Delta:", delta,leaving_arc
        if delta > 0:
            for t in range(1,int(delta+1)):
                new_point.send_one_unit_flow(pivot_cycle)
                new_point.calculate_C2(ex=ex)
                if t == delta:
                    #print "t"
                    #print "Delta:",delta
                    #print new_point.B
                    #print new_point.arc_num_to_arc_nodes(entering_arc)        
                    #print "depth ",new_point.depth
                    #print "thread",new_point.thread
                    #print "pred  ",new_point.pred
                    new_point.update_BLU(leaving_arc,entering_arc)
                    new_point.update_dtp_pi(leaving_arc,entering_arc)
                    new_point.calculate_reduced_costs()
                    #print "R:", new_point.R
                    R.append(new_point)
                #print "X:"
                #print new_point.new_edges_x
                new_point.compute_flow_cost()
                #print "R:", R
                #print "Delta,t:",delta,t
                #print "Leaving arc:",new_point.arc_nodes_to_arc_nums(leaving_arc)
                #print "Leaving arc:", leaving_arc
                #print "Entering arc:",entering_arc
                #print "Entering arc:",new_point.arc_num_to_arc_nodes(entering_arc)
                #print "B:",new_point.B
                #print "U:",new_point.U
                #print "L:",new_point.L
                #print new_point.B
                #print new_point.arc_num_to_arc_nodes(entering_arc)        
                #print "depth ",new_point.depth
                #print "thread",new_point.thread
                #print "pred  ",new_point.pred
                #print new_point.flow_cost1, new_point.flow_cost2
                if new_point.flow_cost2 == 125160:
                    exit()
                #EEP.append(bi_ns.Reduced_Efficient_Extreme_Point(new_point)) #store B,L,U,x,pi_1,pi_2
                #EEP.append(new_point)
                point_for_saving = bi_ns.Efficient_Extreme_Point() 
                EEP.append(point_for_saving)
                point_for_saving.initialize_from_other_point(new_point)
                compute_entering_arcs(point_for_saving,ex)
        else:
            #print "Delta:",delta
            #print "Pivot cycle:",pivot_cycle
            #print "here"
            new_point.update_BLU(leaving_arc,entering_arc)
            new_point.update_dtp_pi(leaving_arc,entering_arc)
            new_point.calculate_reduced_costs()
            #print "X:"
            #print new_point.new_edges_x
            new_point.compute_flow_cost()
            R.append(new_point)
            #EEP.append(bi_ns.Reduced_Efficient_Extreme_Point(new_point)) #store B,L,U,x,pi_1,pi_2
            point_for_saving = bi_ns.Efficient_Extreme_Point()
            EEP.append(point_for_saving)
            point_for_saving.initialize_from_other_point(new_point)
        #print new_point.B
        #print new_point.arc_num_to_arc_nodes(entering_arc)        
        #print "depth ",new_point.depth
        #print "thread",new_point.thread
        #print "pred  ",new_point.pred
        #print new_point.flow_cost1, new_point.flow_cost2
        #if new_point.flow_cost2 == 125160:
            #exit()
            
def find_candidate_arc(List):
    """Returns the candidate arc of all efficient points in the
    EIP algorithm"""
    i = 0
    long_list = len(List)
    while i < long_list and not List[i].R:   # To be changed for "Last Point" for improving performance
        i += 1
    if i == long_list:
        return False
    value = List[i].flow_cost1 + List[i].R[0][1]
    point = List[i]
    for element in List[i:]:
        if element.R:
            if value > element.flow_cost1 + element.R[0][1]:
                point = element
                value = element.flow_cost1 + element.R[0][1]
    return point

def non_dominated_R(point,List):
    """Evaluates if f(x) + c_ij is dominated or not"""
    index = List.index(point)
    i = 0
    long_list = len(List)
    while index + i < long_list-1:
        if point.flow_cost1 + point.R[0][1] < List[index + i+1].flow_cost1 and point.flow_cost2 + point.R[0][2] < List[index + i].flow_cost2:
            #print point.flow_cost1 + point.R[0][1]
            #print point.flow_cost1
            #print point.R[0][1]
            return True
        #elif point.flow_cost2 + point.R[0][2] >= List[index + i].flow_cost2:
        #    return False
        else:
            i += 1
    return False
   
def reduced_cost(i,c,pi):
    """Return the reduced cost of an edge i.
    """
    c = C[i] - pi[S[i]] + pi[T[i]]
    return c 

def plot_function(EEP):
    points_f1 = []
    points_f2 = []

    for point in EEP:
        fc1, fc2 = point.return_flow_cost()
        points_f1.append(fc1) 
        points_f2.append(fc2)

    plt.figure()
    plt.scatter(points_f1,points_f2)
    for x,y in zip(points_f1, points_f2):
        label = "(" + str(x) + "," + str(y) + ")"
        plt.annotate(label,xy = (x,y))

def plot_brute_solutions(brute_solution):
    plt.figure()
    plt.scatter(brute_solution[1],brute_solution[2])
    for x,y in zip(brute_solution[1], brute_solution[2]):
        label = "(" + str(x) + "," + str(y) + ")"
        plt.annotate(label,xy = (x,y))

def new_net_simplex_examples():
    #New inplementation of network simplex
    n = 6 #amount of nodes
    #edges = [(0,1),(0,2),(1,3),(1,4),(2,3),(2,4),(3,5),(4,5)]
    edges = [(5,1),(5,2),(1,3),(1,4),(2,3),(2,4),(3,0),(4,0)]
    C = [0,0,3,2,3,1,0,0]
    C2 = [0,0,0,0,0,0,0,0]
    u = [1,1,1,1,1,1,2,2]
    #d = [-2,0,0,0,0,2]
    d = [2,0,0,0,0,-2]
    app_nodes = [1,2]
    network_nodes = [3,4]
    in_node = 5
    out_node = 0
    problem = bi_ns.Problem(n,edges,C,C2,u,d)
    problem.network_simplex(app_nodes,network_nodes,in_node,out_node)

    #Another problem
    print "##################################"
    print "Second problem"
    q = 1000
    j = 0
    errors = 0
    for i in range(q):
        n = 7 #amount of nodes
        app_nodes = [1,2,3]
        network_nodes = [4,5,6]
        in_node = 7
        out_node = 0
        edges = []
        max_cost = 10
        C = []
        C2 = []
        u = []
        d = []
        number_of_apps = len(app_nodes)
        for a_node in app_nodes:
            #edges.append((in_node,a_node))
            #C.append(0)
            #C2.append(0)
            #u.append(1)
            for n_node in network_nodes:
                edges.append((a_node,n_node))
                #C.append(np.random.randint(1,10))
                C.append(np.random.rand()*max_cost)    
                C2.append(0)
                u.append(1)
        for n_node in network_nodes:
            edges.append((n_node,out_node))
            C.append(0)
            C2.append(0)
            u.append(number_of_apps)
        #print C
        #print C2
        #print u
        problem = bi_ns.Problem(n,edges,C,C2,u,d)
        problem.network_simplex(app_nodes,network_nodes,in_node,out_node)
        if min(C[0:3]) == problem.C[1][list(problem.points[0].x[1]).index(1)]:
            j += 1
        else:
            errors += 1
            print "error"
            exit()
        if min(C[3:6]) == problem.C[2][list(problem.points[0].x[2]).index(1)]:
            j += 1
        else:
            errors += 1
            print "error"
            exit()
        if min(C[6:9]) == problem.C[3][list(problem.points[0].x[3]).index(1)]:
            j += 1
        else:
            errors += 1
            print "error"
            exit()
    print "Correctness porcentaje:", float(j)/(3*float(q))*100
    print "Errors:", errors
    print "Total tries:", q
    
    print "##################################"
    print "Third problem"
    q = 10
    j = 0
    errors = 0
    for i in range(q):
        n = 35 #amount of nodes
        app_nodes = list(range(1,31))
        network_nodes = [31,32,33,34]
        in_node = None
        out_node = 0
        edges = []
        max_cost = 100
        C = []
        C2 = []
        u = []
        d = []
        number_of_apps = len(app_nodes)
        number_of_networks = len(network_nodes)
        for a_node in app_nodes:
            #edges.append((in_node,a_node))
            #C.append(0)
            #C2.append(0)
            #u.append(1)
            for n_node in network_nodes:
                edges.append((a_node,n_node))
                #C.append(np.random.randint(1,max_cost))
                C.append(np.random.rand()*max_cost)    
                C2.append(0)
                u.append(1)
        for n_node in network_nodes:
            edges.append((n_node,out_node))
            C.append(0)
            C2.append(0)
            u.append(number_of_apps)
        #print C
        #print C2
        #print u
        problem = bi_ns.Problem(n,edges,C,C2,u,d)
        problem.network_simplex(app_nodes,network_nodes,in_node,out_node)
        for k in range(number_of_apps):
            if min(C[4*k:4+4*k]) == problem.C[k+1][list(problem.points[0].x[k+1]).index(1)]:
                j += 1
            else:
                errors += 1
    print "Correctness porcentaje:", float(j)/(number_of_apps*float(q))*100
    print "Errors:", errors
    print "Total tries:", q
    print "Number of apps:",number_of_apps
    print "Number of networks", number_of_networks


def old_examples():
    """Old examples"""
    ###Old examples
    #Forming example graph

    G = nx.DiGraph()
    G.add_node(1, demand=-7)
    G.add_node(4,demand=7)
    G.add_nodes_from([2,3])
    G.add_edge(1,2,weight=-1,capacity=5,weight2=3)
    G.add_edge(1,3,weight=5,capacity=4,weight2=-2)
    G.add_edge(2,3,weight=1,capacity=3,weight2=5)
    G.add_edge(2,4,weight=3,capacity=6,weight2=1)
    G.add_edge(3,2,weight=4,capacity=4,weight2=1)
    G.add_edge(3,4,weight=6,capacity=7,weight2=3)
    
    EEP = IPOEB(G) #Efficient points lying in the efficient boundary
    EIP(EEP) #Efficient points not lying in the efficient boundary
    
    
    #Solving inverse problem

    G = nx.DiGraph()
    G.add_node(1, demand=-7)
    G.add_node(4,demand=7)
    G.add_nodes_from([2,3])
    G.add_edge(1,2,weight=3,capacity=5,weight2=-1)
    G.add_edge(1,3,weight=-2,capacity=4,weight2=5)
    G.add_edge(2,3,weight=5,capacity=3,weight2=1)
    G.add_edge(2,4,weight=1,capacity=6,weight2=3)
    G.add_edge(3,2,weight=1,capacity=4,weight2=4)
    G.add_edge(3,4,weight=3,capacity=7,weight2=6)
    
    #Solving first efficient point by network siplex
    EEP2 = IPOEB(G)
    EIP(EEP2) #Efficient Extreme points not lying in the efficient boundary
    
    #Network oriented problem
 
    m = 4 #number of networks
    n = 3 #number of criteria for MADM
    h = 5 #number of apps
    #Network matrix
    #D = np.array([(150,55,10e-4), 
                #(180,65,10e-4),
                #(100, 50,10e-3)]) #delay, jitter, error rate (n x m)
    D = np.random.rand(m,n)
    print D
    #Vector indicating if it is a parameter to maximize or minimize
    max_or_min = [] # all mins
    for i in range(m):
        max_or_min.append(0)
    #TOPSIS
    W = []
    r = []
    for i in range(h):
        W1 = np.random.rand(n,1)
        W1 = W1/np.sum(W1)
        r1 = MADM.TOPSIS(n, m, W1, D, max_or_min,ret='r')
        for i in range(m):
            if r1[i] == 0:  
                r1[i] = np.min(r1[np.nonzero(r1)])
                print
                print "inf"
        r1 = 1/r1
        W.append(W1)
        r.append(r1)
    print r

    
    G = nx.DiGraph()
    for i in range(2,h+2):      #nodes of apps
        G.add_edge(1,i,weight=0,capacity=1,weight2=0)
    for (i,k) in zip(range(2,h+2),range(h)):
        for (j,l) in zip(range(h+2,h+2+m),range(m)):
            G.add_edge(i,j,weight=float(r[k][l]),capacity=1,weight2=l)
    for i in range(m):
        G.add_edge(h+2+i,h+2+m,weight=0,capacity=h,weight2=0)

    G.node[1]['demand']= -h
    G.node[h+2+m]['demand']= h
    EEP3 = IPOEB(G) #Efficient points lying in the efficient boundary
    #EIP(EEP3)    
    
    print "Network oriented problem results"
    
    
    #Graph tools and plotting
    #print "Efficient points 1st problem"
    #for point in EEP:
    #    print "f1,f2:(", point.flow_cost1,",",point.flow_cost2,")"
    #print "Efficient points 2nd problem"
    #for point in EEP2:
    #    print "f1,f2:(", point.flow_cost1,",",point.flow_cost2,")"
    print "W:", W
    print "Efficient points Network problem"
    for point in EEP3:
        print "f1,f2:(", point.flow_cost1,",",point.flow_cost2,")"
    for point in EEP3:
        print point.new_edges_x
    #nx.network_simplex(G)
    #nx.draw_networkx(G,with_labels=True,node_size=500)
    #plt.show

    #plot_function(EEP)
    #plot_function(EEP2)
    plot_function(EEP3)
    plt.show()

if __name__ == '__main__':
    sys.exit(main())



